package ir.godaarsoft.retrofitdemo.models;

public class Question {
    String title;
    String link;

    @Override
    public String toString() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
