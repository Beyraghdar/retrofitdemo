package ir.godaarsoft.retrofitdemo.models;
import java.util.List;
public class StackOverflowQuestions {
    List<Question> items;

    public List<Question> getItems() {
        return items;
    }

    public void setItems(List<Question> items) {
        this.items = items;
    }
}