package ir.godaarsoft.retrofitdemo.Views;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

import ir.godaarsoft.retrofitdemo.IStackOverflowAPI;
import ir.godaarsoft.retrofitdemo.R;
import ir.godaarsoft.retrofitdemo.models.Question;
import ir.godaarsoft.retrofitdemo.models.StackOverflowQuestions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends ListActivity implements Callback<StackOverflowQuestions> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayAdapter<Question> arrayAdapter = new ArrayAdapter<Question>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                new ArrayList<Question>());
        setListAdapter(arrayAdapter);
        setProgressBarIndeterminateVisibility(true);
        setProgressBarVisibility(true);
    }

    public void onClickQuery(View view)
    {
        setProgressBarIndeterminateVisibility(true);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.stackexchange.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // prepare call in Retrofit 2.5
        IStackOverflowAPI iStackOverflowAPI = retrofit.create(IStackOverflowAPI.class);
        Call<StackOverflowQuestions> call = iStackOverflowAPI.loadQuestions("android");
        //asynchronous call
        call.enqueue(this);
        // synchronous call would be with execute, in this case you
        // would have to perform this outside the main thread
        // call.execute()
        // to cancel a running request
        // call.cancel();
        // calls can only be used once but you can easily clone them
        //Call<StackOverflowQuestions> c = call.clone();
        //c.enqueue(this);
    }

    @Override
    public void onResponse(Call<StackOverflowQuestions> call, Response<StackOverflowQuestions> response) {
        setProgressBarIndeterminateVisibility(false);
        ArrayAdapter<Question> adapter = (ArrayAdapter<Question>) getListAdapter();
        adapter.clear();
        adapter.addAll(response.body().getItems());
    }

    @Override
    public void onFailure(Call<StackOverflowQuestions> call, Throwable t) {
        Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
